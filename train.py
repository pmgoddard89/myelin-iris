import os
from sklearn import linear_model
from sklearn.datasets import load_iris
import pickle

data_path = os.environ.get('DATA_PATH') or '/tmp/data/'
model_path = os.environ.get('MODEL_PATH') or '/tmp/model/'
if not os.path.exists(model_path):
    os.makedirs(model_path)


def main():
    with open(data_path + "iris_data", 'rb') as fh:
        X, y = pickle.load(fh)

    print("Training model")
    clf = linear_model.LogisticRegression(penalty="l2", C=1.0, solver="lbfgs", random_state=42)
    clf.fit(X, y)
    print('Coefficients: \n', clf.coef_)

    pickle.dump(clf, open(model_path + "iris_logistic_clf.pkl", 'wb'))


if __name__ == "__main__":
    main()