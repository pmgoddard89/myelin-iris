import os
from sklearn.datasets import load_iris
import pickle

data_path = os.environ.get('DATA_PATH') or '/tmp/data/'
if not os.path.exists(data_path):
    os.makedirs(data_path)


def main():
    iris = load_iris()
    X, y = iris.data, iris.target

    # this is where any preprocessing etc would go

    with open(data_path + "iris_data", 'wb') as fh:
        pickle.dump((X, y), fh)


if __name__ == "__main__":
    main()