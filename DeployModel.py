import os
import pickle
from myelin import metric

# we pick up from local fs
model_path = os.environ.get('MODEL_PATH') or '/tmp/model/'


class DeployModel(object):
    '''
    predict and send_feedback are defined by the seldon image builder
    discuss this with tamas again in future when i have ideas of wanting to attach other endpoints
    - there is an endpoint for predict, and send feedback associated with each model
    '''
    def __init__(self):
        self.model = pickle.load(open(model_path + "iris_logistic_clf.pkl", 'rb'))
        self.c = metric.MetricClient()

    def predict(self, X, feature_names):
        predictions = self.model.predict(X)
        return predictions

    def send_feedback(self, features, feature_names, reward, truth):
        res = self.c.post_update("deploy_accuracy", reward)
        print("Posted metric with status code: %s" % res.status_code)


if __name__ == '__main__':
    d = DeployModel()